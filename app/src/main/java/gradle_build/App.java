package gradle_build;

import java.util.Scanner;

import gradle_build.entity.ContainerSquare;
import gradle_build.entity.StockProfit;
import gradle_build.entity.Permutations;

public class App {
    public static void main(String[] args) {
        try (Scanner numberScanner = new Scanner(System.in)) {
            System.out.print("Choose (1 - Permutations, 2 - StockProfit, 3 - ContainerSquare): ");
            int option = numberScanner.nextInt();
            if (option == 1) {
                Permutations.main(args);
                return;
            } else if (option == 2) {
                StockProfit.main(args);
                return;
            } else if (option == 3) {
                ContainerSquare.main(args);
                return;
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("Incorrect number");
        }
    }
}
